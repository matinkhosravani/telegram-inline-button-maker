<?php


namespace app;


class TelegramEnvExample
{
    const BOT_TOKEN = '';
    const BOT_NAME = '';
    const API_URL = "https://api.telegram.org/bot" . self::BOT_TOKEN;
    const ADMIN_CHATID = 0;
    const WEBHOOK_URL = "";
    const DB_NAME = "";
    const CHANNEL_NAME = "";

}