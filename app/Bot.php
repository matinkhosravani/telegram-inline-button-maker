<?php

namespace app;


class Bot
{


    public function __construct()
    {

    }


    /**
     * @param $url
     */
    public function setWebHook($url)
    {
        $this->superFunction('setwebhook', ["url" => $url]);
    }

    public function sendTextMessage($chatId, $text)
    {

        return $this->superFunction('sendmessage', ['chat_id' => $chatId, 'text' => $text]);
    }

    public function sendMessageWithInlineKeyboard($chatId, $text, $replyMarkup)
    {
        return $this->superFunction('sendmessage', ['chat_id' => $chatId, 'text' => $text, 'reply_markup' => $replyMarkup]);
    }

    /**
     * @param $chatId
     * @param $fromChatId
     * @param $messageId
     */
    public function forwardMessage($chatId, $fromChatId, $messageId)
    {
        $this->superFunction('forwardMessage',
            [
                'chat_id' => $chatId,
                'from_chat_id' => $fromChatId,
                'message_id' => $messageId
            ]
        );
    }

    public function sendPhotoWithInlineKeyboard($photoId , $caption , $fromId , $keyboard)
    {
        $this->superFunction("sendphoto",
            [
                "photo" => $photoId,
                "caption" => $caption,
                "chat_id" => $fromId,
                "reply_markup" => $keyboard
            ]
        );

    }

    /**
     * @return mixed
     */
    public function getUpdate()
    {
        $update = file_get_contents('php://input');
        if ($update == '')
            die();
        return json_decode($update);
    }


    /**
     * @param $method
     * @param array $datas
     * @return mixed
     */
    private function superFunction($method, array $datas = [])
    {
        $bot = TelegramEnv::API_URL . '/' . $method;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $bot);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datas);
        $result = curl_exec($ch);
        if (curl_error($ch)) {
            var_dump(curl_error($ch));
        } else {
            curl_close($ch);
            return json_decode($result);
        }
    }

    /**
     * @param $chatId
     * @param $userId
     * @return bool
     */
    public function getChatMember($chatId, $userId) :bool
    {
        return $this->superFunction('getchatmember' , ['chat_id' => $chatId ,'user_id' => $userId ])->result->status != 'left' ;
    }

    public function answerInlineQuery($inlineQueryId, $results, $cacheTime = 360)
    {

        $this->superFunction("answerInlineQuery",
            [
                "inline_query_id" => $inlineQueryId,
                "results" => $results,
                "cache_time" => $cacheTime,

            ]
        );
    }

}