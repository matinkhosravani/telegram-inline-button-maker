<?php


namespace app;


trait Helpers
{
    /**
     * @param $args
     * @return false|string
     */
    public static function createInLineKeyboard($args)
    {
        $keyboard =
            [
                "inline_keyboard" => $args,
                "one_time_keyboard" => true,
                "selective" => true,
                "resize_keyboard" => true
            ];
        return $keyboard = json_encode($keyboard, true);

    }


    public static function isUrl($string)
    {
        $regex = "((https?|ftp)\:\/\/)?"; // SCHEME
        $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
        $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP
        $regex .= "(\:[0-9]{2,5})?"; // Port
        $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
        $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
        $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor

        if (preg_match("/^$regex$/i", $string)) // `i` flag for case-insensitive
        {
            return true;
        } else
            return false;

    }

    public static function makeButton($result, $buttonRow, $buttonColumn ,$idenString)
    {
        $args = [];
        $rowsOfButton = [];
        for ($j = 0; $j < $buttonRow; $j++) {
            for ($i = 0; $i < $buttonColumn; $i++) {
                $loop_invariant = (($j * $buttonColumn * 2) + $i % ($buttonColumn * 2)) + $i;
                if (self::isUrl($result[$loop_invariant + 1])) {
                    array_push($rowsOfButton, ["text" => $result[$loop_invariant], "url" => $result[$loop_invariant + 1]]);
                } else {
                    array_push($rowsOfButton, ["text" => $result[$loop_invariant], "callback_data" => $result[$loop_invariant + 1]]);
                }
            }
            array_push($args, $rowsOfButton);
            for ($i = 0; $i <= $buttonRow * 2; $i++) {
                unset($rowsOfButton[$i]);
            }
            $rowsOfButton = array_values($rowsOfButton);

        }
        array_push($args, [
            [
                "text" => "Publish", "switch_inline_query" => $idenString
            ]

        ]);

        return $args;

    }

    /**
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 10) :string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}