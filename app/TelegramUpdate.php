<?php


namespace app;


class TelegramUpdate
{
    public $messageId;
    public $fromId;
    public $isBot;
    public $firstName;
    public $username;
    public $chatId;
    public $chatType;
    public $messageText;
    public $messageDate;
    //
    public $callbackQuery;
    public $callbackQueryData;
    //
    public $document;
    public $photo;
    public $photoId;
    //
    public $inlineQuery;
    public $query;
    public $queryId;
    public $queryFromId;


    public function __construct($update)
    {

        $this->initMessage($update);
        $this->initCallBackQuery($update);
        $this->initDocument($update);
        $this->initPhoto($update);
        $this->initInlineQuery($update);

    }

    private function initInlineQuery($update)
    {

        if (isset($update->inline_query)) {
            $this->inlineQuery = $update->inline_query;
            $this->query = $this->inlineQuery->query;
            $this->queryId = $this->inlineQuery->id;
            $this->queryFromId = $this->inlineQuery->from->id;
        }
    }

    private function initPhoto($update)
    {
        if (isset($update->message->photo)) {
            $this->photo = $update->message->photo;
            $this->photoId = $update->message->photo[0]->file_id;
        }
    }

    private function initDocument($update)
    {
        if (isset($update->message->document)) {
            $this->document = $update->message->document;
        }
    }

    private function initCallBackQuery($update)
    {
        if (isset($update->callback_query)) {
            $this->callbackQueryData = $update->callback_query->data;
            $this->callbackQuery = $update->callback_query;
        }
    }

    private function initMessage($update)
    {
        if (isset($update->message) || isset($update->callback_query)) {
            $this->messageId = $update->message->message_id ?? $update->callback_query->message->message_id;
            $this->fromId = $update->message->from->id ?? $update->callback_query->from->id ?? $update->inlineQuery->from->id;
            $this->isBot = $update->message->is_bot ?? $update->callback_query->from->is_bot;
            $this->firstName = $update->message->from->first_name ?? $update->callback_query->from->first_name;
            $this->username = $update->message->from->username ?? $update->callback_query->from->username;
            $this->chatId = $update->message->chat->id ?? $update->callback_query->message->chat->id;
            $this->chatType = $update->message->chat->type ?? $update->callback_query->message->chat->type;
            $this->messageText = $update->message->text;
            $this->messageDate = $update->message->date ?? $update->callback_query->message->date;
        }
    }


}