<?php


namespace app;


use PDO;

class Database extends PDO
{
    private static $db = null;


    public function __construct()
    {
        $dsn = "mysql:host=mariadb;dbname=button_maker";
        $username = "root";
        $passwd = "123456";
        parent::__construct($dsn, $username, $passwd);


    }

    /**
     * @return Database|null
     */
    public static function getInstance()
    {
        if (self::$db == null)
        {
            self::$db = new Database();
        }

        return self::$db;
    }

}