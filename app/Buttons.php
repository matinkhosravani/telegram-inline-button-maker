<?php


namespace app;

class Buttons
{
    use Helpers;

    /**
     * @return false|string
     */
    public static function welcome()
    {
        return self::createInLineKeyboard([
            [
                ["text" => "ارتباط با ادمین", "callback_data" => "sendToAdmin"]
                ,
                ["text" => " aks_bnw اینستاگرام", "url" => "https://instagram.com/aks_bnw?igshid=325p2sqnzcn4"]
            ],
            [
                ["text" => "ارسال عکس", "callback_data" => "sendPhoto"]
            ]
        ]);

    }

}
