<?php


namespace app;


class Steps
{
    const INSERTING_USER = 0;
    const GETTING_TITLE = 1;
    const GETTING_BUTTON_NUMBER = 2;
    const GETTING_ROW_COL_NUMBER = 3;
    const GETTING_BUTTON_DATA = 4;
    const FINISH = 5;

}