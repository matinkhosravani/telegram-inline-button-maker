<?php


namespace app;


class Messages
{


    /**
     * @param $firstName
     * @return string
     */
    public static function welcome($firstName)
    {
        $welcomeText = $firstName . "عزیز" .
            " Welcome to the Button Maker Bot  \n " .
            "Please Send your Title \n" .
            "خوش آمدید لطفا تیتر دکمه های خود را مشخص کنید !‌" .
            "\n" .
            "اگر قبلا از روبات استفاده کردید برای استفاده دوباره /cancel  رو بفرستید !";

        return $welcomeText;
    }

    /**
     * @return string
     */
    public static function default()
    {
        return  "لطفا یکی از گزینه های زیر را انتخاب کنید و یا از دکمه cancel/ استفاده کنید";
    }

    /**
     * @return string
     */
    public static function sendToAdmin()
    {
        return "لطفا پیام خود را ارسال کنید \n پیام به صورت ناشناس ارسال میشود";

    }

    /**
     * @return string
     */
    public static function sendPhoto()
    {
        return "لظفا عکس خود را ارسال نمایید" ;
    }

    /**
     * @return string
     */
    public static function thanksForSend()
    {
        return "✅عکس شما تحویل ادمین داده شد\n ✅برای ارسال دوباره, عکس  خود را بفرستید \n ✅برای ارتباط با ادمین pm/ را بزنید\n ℹ️ در صورت علاقه برا ادمین شدن در اینستاگرام لطفا آیدی خود را در بخش ارتباط با ادمین بفرستید" ;
    }

    /**
     * @return string
     */
    public static function sendNewPhoto()
    {
        return 'لطفا عکس خود را بفرستید';
    }

    /**
     * @return string
     */
    public static function cancel()
    {
        return "Type /start to try again \n ".
            "برای استفاده دوباره دکمه استارت را بزنید";
    }

    /**
     * @return string
     */
    public static function sentToAdmin()
    {
        return "پیام شما به صورت ناشناس برای ادمین فرستاده شد ";
    }

    public static function sendNumberButton()
    {
        return "Send number of your buttons\n" .
            "تعداد دکمه های خود را به شکل یک عدد بفرستید ! ";
    }

    public static function sendCaption()
    {
        return "Send Caption of your photo\n" .
            "کپشن عکس خود را بفرستید";

    }

    public static function rowOk()
    {
        return  " OK !\n Enter the text of your buttons in this Format (Separate them with ,) : \n" .
            "Text,Data or (URL),Text,Data \n" .
            "Example For 2 Buttons  : \n" .
            "MywebSite,MyWebURL ,myButton,MycallbackData".
            "خب !‌ متن دکمه خود را در قالب زیر بفرستید\n".
            "متن , ادرس یا داده\n".
            " \nمثال برای دو دکمه :".
            "MywebSite,MyWebURL ,myButton,MycallbackData"
            ;
    }

    public static function rowNotOk()
    {
        return  " Your row and Column are not valid " .
            "/cancel For restart";
    }

    public static function rowAndColumnError()
    {
        return  " Your Text and data is incorrect !\n"
            . "Send me Again\n" .
            "/cancel For restart";
    }

    public static function numberOfRowAndColumnError()
    {
        return " Your input is invalid Please Enter your Row and Column in this Format\n "
            . "ROW,COLUMN\n" .
            "Example : \n" .
            "2,4\n" .
            "تعداد سطر ها و ستون های خود را اشتباه وارد کردید!‌";
    }


    public static function enterNumberOfRowAndColumn()
    {
        return " Enter Number of row and Column in this Format \n "
            . "ROW,COLUMN.\n" .
            "تعداد سطر ها و ستون های دکمه های خود را در غالب زیر بفرستید\n"
            . "تعدادسطر,تعداد ستون";
    }

    public static function numberButtonError()
    {
        return "Send number of your buttons\n" .
            "تعداد دکمه های خود را به شکل یک عدد بفرستید ! ";

    }

    public static function joinChannel()
    {
        return "to use this bot please subscribe this channel\n".
            "برای استفاده ازین ربات لطفا عضو کانال زیر شوید !\n".
            TelegramEnv::CHANNEL_NAME;
    }


    public static function sendInstruction($idenString)
    {
       return "you can send this button by mentioning this code with bot name \n".
           "@Buttoninlinebot ".$idenString ."\n".
           "شما میتوانید برای فرستادن دکمه ربات را با کد زیر منشن کنید  \n".
            " @Buttoninlinebot ".$idenString ."\n";

    }

}