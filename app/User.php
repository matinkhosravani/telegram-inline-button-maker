<?php


namespace app;


use PDO;

class User
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    public function selectWhere($select, $whereColumn , $whereValue)
    {
        $user = $this->db->prepare("SELECT " . $select . " FROM users WHERE $whereColumn='$whereValue' AND is_completed = 0");
//        $user->bindValue(1, $whereValue, PDO::PARAM_INT);
        $user->execute();

        return $user->fetch()[0];
    }

    public function selectForQuery($select, $whereColumn , $whereValue)
    {
        $user = $this->db->prepare("SELECT " . $select . " FROM users WHERE $whereColumn='$whereValue'");
//        $user->bindValue(1, $whereValue, PDO::PARAM_INT);
        $user->execute();

        return $user->fetch()[0];
    }


    public  function isExist($fromId)
    {
        $user = $this->db->prepare("SELECT count( id )  FROM users WHERE id = ? ");
        $user->bindValue(1, $fromId, PDO::PARAM_INT);
        $user->execute();

        return $user->fetch()[0] > 0;

    }

    public function updateWhere($column , $value , $whereColumn , $whereValue)
    {
        $result = $this->db->prepare("UPDATE users SET $column = '$value' WHERE $whereColumn = '$whereValue'  AND is_completed = 0");
//        $result->bindValue(1, $whereValue, PDO::PARAM_INT);
        $result->execute();

    }

    public function insert($fromId, $step , $iden)
    {
        $result = $this->db->prepare("INSERT INTO users (id , step , iden) VALUES ( ?  , ?  , ? )");
        $result->bindValue(1, $fromId, PDO::PARAM_INT);
        $result->bindValue(2, $step, PDO::PARAM_INT);
        $result->bindValue(3, $iden, PDO::PARAM_STR);

        $result->execute();

    }


    function resetValues($fromId)
    {
        $result = $this->db->prepare("DELETE FROM users WHERE id = ? AND is_completed = 0");
        $result->bindValue(1, $fromId, \PDO::PARAM_INT);
        $result->execute();

    }



}