<?php


namespace app;


use app\Messages;
use PDO;


class BotLogic
{
    use Helpers;

    private $update;
    private $bot;
    private $db;
    private $user;
    private $idenString;

    public function __construct(TelegramUpdate $update, Bot $bot)
    {
        $this->update = $update;
        $this->bot = $bot;

        $this->joinBeforeUse($update);


        $this->user = new User();
        $this->idenString = $this->user->selectWhere('iden' ,'id' , $update->fromId);
        $step = $this->user->selectWhere('step','id', $update->fromId) ?? 0;

        while (true) {
            if ($update->messageText == "/start" && $step == Steps::INSERTING_USER) {
                $this->idenString = $this->generateRandomString();
                $this->insertUser($update , $this->idenString);
                break;
            } elseif ($update->messageText == "/cancel") {
                $this->cancel($update);
                break;
            } elseif ($step == Steps::GETTING_TITLE) {
                if (!isset($update->photo))
                    $this->saveTitle($update);
                else
                    $this->savePhoto($update);
                break;
            } else if ($step == 10) {
                $this->saveCaption($update);
                break;
            } else if ($step == Steps::GETTING_BUTTON_NUMBER) {
                $this->saveButtonNumber($update);
                break;
            } else if ($step == Steps::GETTING_ROW_COL_NUMBER) {
                $this->saveRowAndCol($update);
                break;
            } else if ($step == Steps::GETTING_BUTTON_DATA) {
                $this->saveButtonData($update , $this->idenString);
                break;
            }

            if (isset($update->inlineQuery)) {
                $this->publishButton($update);
                break;
            }
            break;
        }
    }

    private function insertUser(TelegramUpdate $update , $iden)
    {
//        $this->user->isExist($update->fromId) ? $this->user->updateWhere('step', Steps::GETTING_TITLE,'id', $update->fromId) :
        $this->user->insert($update->fromId, Steps::GETTING_TITLE , $iden);
        $this->bot->sendTextMessage($update->fromId, Messages::welcome($update->firstName));
    }

    private function cancel(TelegramUpdate $update)
    {
        $this->user->resetValues($update->fromId);
        $this->bot->sendTextMessage($update->fromId, Messages::cancel());
    }

    private function saveTitle(TelegramUpdate $update)
    {
        $this->user->updateWhere('step', Steps::GETTING_BUTTON_NUMBER, 'iden', $this->idenString);
        $this->user->updateWhere('title', $update->messageText,'iden',  $this->idenString);
        $this->bot->sendTextMessage($update->fromId, Messages::sendNumberButton());
    }

    private function savePhoto(TelegramUpdate $update)
    {
        $this->user->updateWhere('isPhoto', 'y', 'iden', $this->idenString);
        $this->user->updateWhere('photoId', $update->photoId, 'iden', $this->idenString);
        $this->user->updateWhere('step', 10, 'iden',$this->idenString);
        $this->bot->sendTextMessage($update->fromId, Messages::sendCaption());
    }

    private function saveCaption(TelegramUpdate $update)
    {
        if ($update->messageText != "none")
            $this->user->updateWhere('caption', $update->messageText,'iden',  $this->idenString);
        else
            $this->user->updateWhere('caption', '','id',  $update->fromId);
        $this->user->updateWhere('step', Steps::GETTING_BUTTON_NUMBER, 'iden', $this->idenString);
        $this->bot->sendTextMessage($update->fromId, Messages::numberButtonError());
    }

    private function saveButtonNumber(TelegramUpdate $update)
    {
        if (ctype_digit($update->messageText)) {
            $this->user->updateWhere('buttonNumber', $update->messageText, 'iden', $this->idenString);
            $this->user->updateWhere('step', Steps::GETTING_ROW_COL_NUMBER,'iden',  $this->idenString);
            $this->bot->sendTextMessage($update->fromId, Messages::enterNumberOfRowAndColumn());
        } else
            $this->bot->sendTextMessage($update->fromId, Messages::numberOfRowAndColumnError());
    }

    private function saveRowAndCol(TelegramUpdate $update)
    {
        $result = explode(",", $update->messageText);// Array of Row and Column
        //todo validate
        $this->user->updateWhere('rowNumber', $result[0],'iden',  $this->idenString);
        $this->user->updateWhere('columnNumber', $result[1], 'iden', $this->idenString);

        $buttonNumber = $this->user->selectWhere('buttonNumber', 'iden', $this->idenString);
        $buttonRow = $this->user->selectWhere('rowNumber','iden', $this->idenString);
        $buttonColumn = $this->user->selectWhere('columnNumber', 'iden', $this->idenString);

        if ($buttonRow * $buttonColumn == $buttonNumber) {
            $this->user->updateWhere('step', Steps::GETTING_BUTTON_DATA,'iden',  $this->idenString);
            $this->bot->sendTextMessage($update->fromId, Messages::rowOk());
        } else
            $this->bot->sendTextMessage($update->fromId, Messages::rowNotOk());
    }

    private function saveButtonData(TelegramUpdate $update , $idenString)
    {
        $buttonNumber = $this->user->selectWhere('buttonNumber','iden',  $this->idenString);
        $buttonRow = $this->user->selectWhere('rowNumber','iden', $this->idenString);
        $buttonColumn = $this->user->selectWhere('columnNumber','iden', $this->idenString);

        $result = explode(",", $update->messageText); //Array of Data and Text
        if (count($result) == ($buttonNumber * 2)) {
            $args = $this->makeButton($result, $buttonRow, $buttonColumn , $idenString);
        } else {
            $this->user->updateWhere('step', Steps::GETTING_ROW_COL_NUMBER,'iden', $this->idenString);
            $this->bot->sendTextMessage($update->fromId, Messages::rowAndColumnError());
            return;
        }

        $finalResult = $this->createInLineKeyboard($args);
        $this->user->updateWhere('kb', $finalResult,'iden',  $this->idenString);
        $this->user->updateWhere('step', Steps::FINISH,'iden',  $this->idenString);
        $title = $this->user->selectWhere('title', 'iden', $this->idenString);
        $isPhoto = $this->user->selectWhere('isPhoto', 'iden', $this->idenString);
        if ($isPhoto != 'y') {
            $this->bot->sendMessageWithInlineKeyboard($update->fromId, $title, $finalResult);
        } else {
            $photoId = $this->user->selectWhere('photoId', 'iden', $this->idenString);
            $caption = $this->user->selectWhere('caption', 'iden', $this->idenString);
            $this->bot->sendPhotoWithInlineKeyboard($photoId, $caption, $update->fromId, $finalResult);
        }
        $this->user->updateWhere('is_completed' , 1 ,'id',$update->fromId);
        $this->bot->sendTextMessage($update->fromId ,Messages::sendInstruction($idenString) );

    }

    private function createResultForText(TelegramUpdate $update, $keyboard)
    {
        return
            [
                [
                    'type' => 'article',
                    'id' => '1',
                    'title' => "Buttons",
                    'message_text' => $this->user->selectForQuery('title','iden',  $update->query),
                    'reply_markup' => $keyboard,
                    'description' => "Tap To Send a Button"
                ]
            ];
    }

    private function createResultForPhoto(TelegramUpdate $update, $keyboard)
    {
        return
            [
                [
                    'type' => 'photo',
                    'photo_file_id' => $this->user->selectForQuery('photoId', 'iden',  $update->query),
                    'id' => '2',
                    'title' => "Buttons",
//                      'message_text' => $connect->query("SELECT title FROM userInfo WHERE ID = $update->fromId")->fetch_all()[0][0],
                    'reply_markup' => $keyboard,
                    'description' => "Tap To Send a Button",
                    'caption' => $this->user->selectForQuery('caption', 'iden',  $update->query),
                ]
            ];
    }

    private function publishButton(TelegramUpdate $update)
    {
        $keyboard = $this->user->selectForQuery('kb','iden',  $update->query);
        $keyboard = json_decode($keyboard, true);
        unset($keyboard["inline_keyboard"][count($keyboard["inline_keyboard"]) - 1]);
        $isPhoto = $this->user->selectForQuery('isPhoto','iden',  $update->query);
        if ($isPhoto != 'y')
            $result = $this->createResultForText($update, $keyboard);
        else
            $result = $this->createResultForPhoto($update, $keyboard);
        $results = json_encode($result, true);
        $this->bot->answerInlineQuery($update->inlineQuery->id, $results);
    }

    private function joinBeforeUse(TelegramUpdate $update)
    {
        if (!$this->bot->getChatMember(TelegramEnv::CHANNEL_NAME, $update->fromId)) {
            $this->bot->sendTextMessage($update->fromId, Messages::joinChannel());
            die();
        }
    }


}


