<?php

require_once 'vendor/autoload.php';

use app\Bot;
use app\BotLogic;
use app\TelegramUpdate;


$bot = new Bot();

$update = $bot->getUpdate();
$update = new TelegramUpdate($update);
$botLogic = new BotLogic($update , $bot);

?>
